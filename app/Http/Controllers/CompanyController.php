<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceCompany;
use Validator;
class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $index = 1;
        $companies = InsuranceCompany::orderBy("company_name","asc")->get();
        return view("pages.companies.company",compact("index","companies"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("pages.companies.addcompany");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),[
                'company_name'         =>  'required',
                'company_logo'     =>  'required|file|mimes:jpg,jpeg,png',
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        $company = new InsuranceCompany();
        if($request->hasFile('company_logo')){
            $fileName = $request->company_logo->getClientOriginalName();  
            $request->company_logo->move(public_path('storage/media/company/logo'), $fileName);
            $company->company_logo = $fileName;
        }
        $company->company_name = $request->company_name;
        $company->company_short_name = $request->company_short_name;
        $company->save();
        return redirect("company")->with("success","Company created successfully !!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $index = 1;
        $company = InsuranceCompany::findOrFail($id);
        return view("pages.companies.showcompany",compact("company",'index'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = InsuranceCompany::findOrFail($id);
        return view("pages.companies.editcompany",compact("company"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = InsuranceCompany::findOrFail($id);
        $validator = Validator::make(
            $request->all(),[
                'company_name'         =>  'required',
            ]
        );
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
        if($request->hasFile('company_logo')){
            $fileName = $request->company_logo->getClientOriginalName();  
            $request->company_logo->move(public_path('storage/media/company/logo'), $fileName);
            $company->company_logo = $fileName;
        }

        $company->company_name = $request->company_name;
        $company->company_short_name = $request->company_short_name;
        $company->save();
        return redirect("company")->with("success","Company updated successfully !!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
