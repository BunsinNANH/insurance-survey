<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceCompany;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware("auth");
    }

    public function index()
    {
        $companies = InsuranceCompany::where('active',1)->with("respondent")->get();
        $getCompanies = InsuranceCompany::all();
        $companiesList = [];
        $countData = [];
        // $topBank = [];
        $rates = [];
        $topRate = 0;
        foreach($companies as $company){
            array_push($companiesList,$company->company_short_name);
            array_push($countData,count($company["respondent"]));
            array_push($rates,$company->answers()->pluck("answer")->sum());
        }
        for ($index=0; $index < count($rates) ; $index++) {
            if ($topRate < $rates[$index]) {
                $topRate = $rates[$index];
            }
        }
        return view("pages.dashboard.dashboard")
        ->with('companiesList',json_encode($companiesList,JSON_NUMERIC_CHECK))
        ->with('rates',json_encode($rates,JSON_NUMERIC_CHECK))
        ->with('countData',json_encode($countData,JSON_NUMERIC_CHECK));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
