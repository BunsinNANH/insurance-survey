<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Respondent;
use App\Models\Answer;
use Validator;
use DB;
class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionsList = $request->question_id;
        $validator = Validator::make(
            $request->all(),[
                'fname'         => 'required',
                'lname'         => 'required',
                'phone'         => 'required',
                'email'         => 'required',
                'my_insurance'  => 'required',
                'answers_1'     => 'required',
                'answers_2'     => 'required',
                'answers_3'     => 'required',
                'answers_4'     => 'required',
                'answers_5'     => 'required',
                'answers_6'     => 'required',
                'answers_7'     => 'required',
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->input());
        }
       
        $exists = DB::table("respondents")->where('email',$request->email)->exists();
        if($exists == true){
            $respondent = Respondent::where("email",$request->email)->first();
            $respondent_id = $respondent->id;
            $respondent->companies()->attach($request->my_insurance);
        }else{
            $respondent = new Respondent();
            $respondent->fname = $request->fname;
            $respondent->lname = $request->lname;
            $respondent->phone = $request->phone;
            $respondent->email = $request->email;
            $respondent->save();
            $respondent->companies()->attach($request->my_insurance);
            $respondent_id = $respondent->id;
        }
        $answers = [
            $request->answers_1,$request->answers_2,
            $request->answers_3,$request->answers_4,
            $request->answers_5,$request->answers_6,
            $request->answers_7,
        ];
        for ($i=0; $i < count($questionsList); $i++) { 
            $answer = new Answer();
            $answer->answer = $answers[$i];
            $answer->save();
            $answer->companies()->attach($request->my_insurance);
            $answer->questions()->attach($questionsList[$i]);
            $answer->respondent()->attach($respondent_id);
        }
        return redirect("/survey-success");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
