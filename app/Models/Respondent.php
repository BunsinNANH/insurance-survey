<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Respondent extends Model
{
    use HasFactory;
    protected $table='respondents';
    protected $primarykey='id';
    protected $fillable = [
        'fname','lname','email','phone'
    ];
    // one respondent belong to many answers
    public function answers(){
        return $this->belongsToMany(Answer::class);
    }
    // one respondent belong to many companies
    public function companies(){
        return $this->belongsToMany(InsuranceCompany::class);
    }
}
