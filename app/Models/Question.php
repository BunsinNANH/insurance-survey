<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    protected $table='questions';
    protected $primarykey='id';
    protected $fillable = [
        'question',
    ];
    
    // one question belong to many answers
    public function answers(){
        return $this->belongsToMany(Answer::class);
    }
}
