<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    protected $table='answers';
    protected $primarykey='id';
    protected $fillable = [
        'answer',
    ];
    
    // one answer belong to many questions
    public function questions(){
        return $this->belongsToMany(Question::class);
    }
    // one answer belong to many companies
    public function companies(){
        return $this->belongsToMany(InsuranceCompany::class);
    }
    // one answer belong to many respondent
    public function respondent(){
        return $this->belongsToMany(Respondent::class);
    }
}
