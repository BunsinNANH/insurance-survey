<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsuranceCompany extends Model
{
    use HasFactory;
    protected $table='insurance_company';
    protected $primarykey='id';
    protected $fillable = [
        'company_name','company_logo',"company_short_name"
    ];
    // one bank belong to many respondent
    public function respondent(){
        return $this->belongsToMany(Respondent::class);
    }
    // one bank belong to many answers
    public function answers(){
        return $this->belongsToMany(Answer::class);
    }
}
