@extends('layouts.front')
@section('frontpage')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                <div class="card">
                    <div class="card-body shadow-sm text-center">
                        <img class="mb-3" src="{{ asset("storage/media/materials/Thankyou-iconV2.png") }}"
                        alt="Thank You Image" width="30%">
                        <h1 class="text-success">Thank You !!</h1>
                        <p class="text-secondary">Your submission has been received.</p>
                        <hr>
                        <a href="{{ url("/") }}" class="btn btn-back btn-lg">
                            <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                            Bank to Home
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection