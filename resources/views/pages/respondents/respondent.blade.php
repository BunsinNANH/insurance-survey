@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <h2 class="mt-4">Respondents Listing</h2>
        <table class="table table-striped table-horvered table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($respondents as $respondent)
                    <tr>
                        <td>{{ $index++ }}</td>
                        <td>{{ $respondent->lname }} {{ $respondent->fname }}</td>
                        <td>{{ $respondent->email }}</td>
                        <td>{{ $respondent->phone }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection