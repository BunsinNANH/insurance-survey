@extends('layouts.dashboard')
@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Update Company</h2>
                </div>
                <div class="card-body">
                    <form action="{{ route("company.update",$company->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method("PATCH")
                        <div class="form-group mt-2">
                            <label for="#company-name">Company Name</label>
                            <input type="text" name="company_name" id="company-name" class="form-control" 
                            placeholder="Company Name " value="{{ $company->company_name }}">
                            @if ($errors->has('company_name'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-3">
                            <label for="#company-short-name">Company Short Name(Optional)</label>
                            <input type="text" name="company_short_name" id="company-short-name" class="form-control" 
                            placeholder="Company Short Name" value="{{ $company->company_short_name }}">
                        </div>
                        <div class="form-group mt-3">
                            <label for="#company-logo">Company Logo</label>
                            <input type="file" name="company_logo" id="company-logo" class="form-control" 
                            placeholder="Company Logo">
                            @if ($errors->has('company_logo'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('company_logo') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-3">
                            <a href="{{ url("company") }}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection