<style>
    .company-logo{
        width:30px;
        height: 30px;
        box-shadow: 2px;
        border-radius: 50px;
    }
</style>
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>	
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <strong>{{ $message }}</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>	
            </div>
        @endif
        <div class="row mt-3">
            <div class="col-md-10"><h2>Insurance Company</h2></div>
            <div class="col-md-2">
                <a href="{{ route("company.create") }}" class="btn btn-sm btn-primary float-right">
                    <i class="fa fa-plus-circle"></i> Add New Company
                </a>
            </div>
        </div>
        <hr>
        <table class="table table-striped table-hovered table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Company Name</th>
                    <th width="30%">Company Short Name</th>
                    <th>Respondent</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($companies as $company)
                    <tr>
                        <td>{{ $index++ }}</td>
                        <td>
                            <img src="{{ asset("storage/media/company/logo/".$company->company_logo) }}" 
                            alt="{{ $company->company_name }} Logo" class="company-logo">
                            {{ $company->company_name }}
                        </td>
                        <td>
                            @if ($company->company_short_name == null)
                                None
                            @else
                                {{ $company->company_short_name }}
                            @endif
                        </td>
                        <td>
                            {{ count(DB::table("insurance_company_respondent")
                            ->where("insurance_company_id",$company->id)->get()) }}
                        </td>
                        <td>
                            <a href="{{ route("company.edit",$company->id) }}">
                                <i class="fas fa-pencil-alt text-success"></i>
                            </a>
                            <a href="#" data-id="{{ $company->id }}" data-name="{{ $company->company_name }}"
                            data-logo="{{ asset("/storage/media/company/logo/".$company->company_logo) }}"
                            data-status="{{ $company->active }}"
                            data-toggle="modal" data-target="#deleteCompany">
                            @if ($company->active == 1)
                                <i class="fas fa-trash-alt text-danger"></i>
                            @else
                                <i class="fa fa-ban text-warning" aria-hidden="true"></i>
                            @endif
                            </a>
                            <a href="{{ route("company.show",$company->id) }}">
                                <i class="fas fa-eye text-info"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deleteCompany" tabindex="-1" role="dialog" aria-labelledby="deleteCompanyLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post" id="form-delete-company">
                    @csrf
                    @method("PUT")
                    <div class="modal-header">
                    <h5 class="modal-title" id="deleteCompanyLabel">Delete Company</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body text-center">
                        <p>Are you sure want to <span class="status text-secondary"></span> 
                            <span class="company-name text-warning"></span> ?
                        </p>
                        <img class="image-logo" src="" width="20%">
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $("#deleteCompany").on('show.bs.modal',function(event){
                var button = $(event.relatedTarget);
                var id = button.data("id");
                var name = button.data("name");
                var logo = button.data("logo");
                var status = button.data("status");
                var modal = $(this);
                var url = "{{ route('deleteCompany.update',':id') }}";
                $(".company-name").text(name);
                $(".image-logo").attr("src",logo);
                console.log(logo)
                if (status == 1) {
                    $(".status").text('activate');
                }else{
                    $(".status").text('disactivate');
                }
                $("#form-delete-company").attr("action",url.replace(":id",id));
            });
        });
    </script>
@endpush