@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <h1 class="mb-3 mt-3">Company Detail</h1>
            <hr>
            <div class="col-lg-4">
                <a href="{{ url('company') }}" class="btn btn-sm btn-warning mb-4">
                    <i class="fas fa-long-arrow-alt-left"></i> Back
                </a>
                <div class="card">
                    <div class="card-body text-center">
                        <img src="{{ asset('storage/media/company/logo/'.$company->company_logo) }}" 
                            alt="{{ $company->company_name }} logo" width="50%">
                        <h1>{{ $company->company_name }}</h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <h2>Respondents Listing</h2>
                <hr>
                <table class="table table-striped table-hovered table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Respondent Name</th>
                            <th>Respondent Email</th>
                            <th>Respondent Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($company["respondent"] as $respondent)
                            <tr>
                                <td>{{ $index++ }}</td>
                                <td>
                                    {{ $respondent->fname }} 
                                    {{ $respondent->lname }}
                                </td>
                                <td>{{ $respondent->email }}</td>
                                <td>{{ $respondent->phone }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection