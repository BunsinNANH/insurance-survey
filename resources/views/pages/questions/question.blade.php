@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <strong>{{ $message }}</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>	
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-block">
                <strong>{{ $message }}</strong>
                <button type="button" class="close" data-dismiss="alert">×</button>	
            </div>
        @endif
        <div class="row mt-3">
            <div class="col-md-10"><h2>Questions</h2></div>
            <div class="col-md-2">
                <a href="{{ route("question.create") }}" class="btn btn-sm btn-primary float-right">
                    <i class="fa fa-plus-circle"></i> Add New Question
                </a>
            </div>
        </div>
        <hr>
        <table class="table table-striped table-hovered table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($questions as $question)
                    <tr>
                        <td>{{ $index++ }}</td>
                        <td>{{ $question->question }}</td>
                        <td>
                            <a href="{{ route("question.edit",$question->id) }}">
                                <i class="fas fa-pencil-alt text-success"></i>
                            </a>
                            <a href="#" data-id="{{ $question->id }}" data-question="{{ $question->question }}"
                            data-status="{{ $question->active }}"
                            data-toggle="modal" data-target="#deleteQuestion">
                            @if ($question->active == 1)
                                <i class="fas fa-trash-alt text-danger"></i>
                            @else
                                <i class="fa fa-ban text-warning" aria-hidden="true"></i>
                            @endif
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="deleteQuestion" tabindex="-1" role="dialog" 
        aria-labelledby="deleteQuestionLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="" method="post" id="form-delete-question">
                    @csrf
                    @method("PUT")
                    <div class="modal-header">
                    <h5 class="modal-title" id="deleteQuestionLabel">Delete Question</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body text-center">
                        <p>Are you sure want to <span class="status text-secondary"></span> 
                            <span class="question text-warning"></span> ?
                        </p>
                    </div>
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){
            $("#deleteQuestion").on('show.bs.modal',function(event){
                var button = $(event.relatedTarget);
                var id = button.data("id");
                var question = button.data("question");
                var status = button.data("status");
                var modal = $(this);
                var url = "{{ route('deleteQuestion.update',':id') }}";
                $(".question").text(question);
                if (status == 1) {
                    $(".status").text('activate');
                }else{
                    $(".status").text('disactivate');
                }
                $("#form-delete-question").attr("action",url.replace(":id",id));
            });
        });
    </script>
@endpush