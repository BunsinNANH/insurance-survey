@extends('layouts.dashboard')
@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card mt-4">
                <div class="card-header">
                    <h2>Update Question</h2>
                </div>
                <div class="card-body">
                    <form action="{{ route("question.update",$question->id) }}" method="POST">
                        @csrf
                        @method("PATCH")
                        <div class="form-group mt-2">
                            <label for="#question">Question</label>
                            <textarea name="question" id="question" cols="30" rows="10"
                            class="form-control" placeholder="Enter New Question">
                                {{ $question->question }}
                            </textarea>
                            @if ($errors->has('question'))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('question') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mt-3">
                            <a href="{{ url("question") }}" class="btn btn-secondary">Cancel</a>
                            <button type="submit" class="btn btn-primary float-right">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection