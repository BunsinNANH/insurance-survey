<style>
    .top-bank-logo{
        width: 30px;
        height: 30px;
        border-radius: 50%;
    }
</style>
@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid mt-4 mb-4 d-flex flex-column justify-content-center">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="chart-container">
                            <div class="pie-chart-container">
                                <canvas id="canvas" height="500" width="600"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 text-center text-white">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card shadow-sm">
                            <div class="card-body bg-warning">
                                <h2><i class="fa fa-university" aria-hidden="true"></i> Companies</h2>
                                <h4><b>{{ count(DB::table("insurance_company")->get()) }}</b></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card shadow-sm">
                            <div class="card-body bg-danger">
                                <h2><i class="fa fa-users" aria-hidden="true"></i> Respondent</h2>
                                <h4><b>{{ count(DB::table("respondents")->get()) }}</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-md-6">
                        <div class="card shadow-sm">
                            <div class="card-body bg-info">
                                <h2><i class="fa fa-question-circle" aria-hidden="true"></i> Questions</h2>
                                <h4><b>{{ count(DB::table("questions")->get()) }}</b></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="mt-4">Insurances Survey Graphic</h2>
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <canvas id="chBar" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js"></script>
    <script>
        $(document).ready(function(){
            // chart colors
            var label = <?php echo $companiesList; ?>;
            var rates = <?php echo $rates; ?>;
            var counts = <?php echo $countData; ?>;
            var colors = ['#D31145','#D52B1D','#00A758','#6F2E8E','#2F3192','#EC008C',
                        '#2D3091','#FFC20E','#009da5','#FFC83D','#002e6e','#004898','#242671','#0067B1',
                        '#0066ab','#0fbcf5','#2D71C4','#b2c224'];
            /* large line chart */
            var chBar = document.getElementById("chBar");
            var chLine = document.getElementById("chLine");
            var chartData = {
                labels: label,
                datasets: [{
                    data: counts,
                    label: "Respondent",
                    backgroundColor: colors,
                    borderColor: colors,
                    borderWidth: 1,
                    pointBackgroundColor: colors
                }]
            };
            if (chBar) {
                new Chart(chBar, {
                    type: 'bar',
                    data: chartData,
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: { beginAtZero: true},
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Respondent'
                                }
                            }],
                            xAxes: [{
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Insurance Company'
                                }
			                }]
                        },
                        legend: { 
                            display: false,
                            position: 'bottom',
                        }
                    }
                });
            }
            // pie chart
            var canvas = document.getElementById("canvas");
            var ctx = canvas.getContext('2d');
            // Global Options:
            Chart.defaults.global.defaultFontColor = 'black';
            Chart.defaults.global.defaultFontSize = 16;
            var data = {
                labels: label,
                datasets: [
                    {
                        fill: true,
                        backgroundColor:colors,
                        data: rates,
                        borderColor:colors,
                        borderWidth: [0,0]
                    }
                ]
            };
            // Notice the rotation from the documentation.
            var options = {
                title: {
                    display: true,
                    text: 'What your favorite insurance company?',
                    position: 'top'
                },
                rotation: -0.7 * Math.PI
            };
            // Chart declaration:
            var myBarChart = new Chart(ctx,{
                type: 'pie',
                data: data,
                options: options
            });
        });
    </script>
@endpush