@extends('layouts.front')
@section('frontpage')
<div class="container mb-4">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="col-md-10 col-sm-12 col-xs-12 col-lg-10">
            <div class="card">
                <div class="card-body shadow-sm">
                    <h1 class="text-center">Survey Form</h1>
                    <form id="regForm" action="{{ route("insurance-survey.store") }}" method="POST"​​> 
                        @csrf
                        @method("POST")
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">First Name <span class="text-danger">*</span></label>
                                    <input type="text" placeholder="First Name..." name="fname"
                                    style="{{ $errors->has('fname') ? 'border-color:red;' : null }}"
                                    value="{{ old('fname') }}">
                                    @if ($errors->has("fname"))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('fname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label for="">Last Name <span class="text-danger">*</span></label>
                                    <input type="text" placeholder="Last Name..." name="lname"
                                    style="{{ $errors->has('lname') ? 'border-color:red;' : null }}"
                                    value="{{ old('lname') }}">
                                    @if ($errors->has("lname"))
                                        <span class="text-danger">
                                            <strong>{{ $errors->first('lname') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Email <span class="text-danger">*</span></label>
                            <input type="email" placeholder="Email..." name="email"
                            style="{{ $errors->has('email') ? 'border-color:red;' : null }}"
                            value="{{ old('email') }}">
                            @if ($errors->has("email"))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="">Phone <span class="text-danger">*</span></label>
                            <input type="tel" placeholder="Phone..." name="phone"
                            style="{{ $errors->has('phone') ? 'border-color:red;' : null }}"
                            value="{{ old('phone') }}">
                            @if ($errors->has("phone"))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group mb-4">
                            <label for="#my_insurance">My Insurance is <span class="text-danger">*</span></label for="">
                            <select name="my_insurance" id="my_insurance" class="form-control"
                            style="{{ $errors->has('my_insurance') ? 'border-color:red;' : null }}">
                                <option selected disabled>Select My Insurance</option>
                                @foreach ($companies as $company)
                                    <option value="{{ $company->id }}">{{ $company->company_name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has("my_insurance"))
                                <span class="text-danger">
                                    <strong>{{ $errors->first('my_insurance') }}</strong>
                                </span>
                            @endif
                        </div>
                        @foreach ($questions as $question)
                            <p>{{ $question->question }} <span class="text-danger">*</span></p>
                            <input type="number" name="question_id[]" value="{{ $question->id }}" hidden>
                            <div class="row mb-4">
                                <div class="custom-radio-button">
                                    <div>
                                        <input type="radio" id="rating-1-{{$index}}" name="answers_{{$index}}" 
                                        value="1">
                                        <label for="rating-1-{{$index}}"><span>1</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-2-{{$index}}" name="answers_{{$index}}" 
                                        value="2">
                                        <label for="rating-2-{{$index}}"><span>2</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-3-{{$index}}" name="answers_{{$index}}" 
                                        value="3">
                                        <label for="rating-3-{{$index}}"><span>3</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-4-{{$index}}" name="answers_{{$index}}" 
                                        value="4">
                                        <label for="rating-4-{{$index}}"><span>4</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-5-{{$index}}" name="answers_{{$index}}" 
                                        value="5">
                                        <label for="rating-5-{{$index}}"><span>5</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-6-{{$index}}" name="answers_{{$index}}" 
                                        value="6">
                                        <label for="rating-6-{{$index}}"><span>6</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-7-{{$index}}" name="answers_{{$index}}" 
                                        value="7">
                                        <label for="rating-7-{{$index}}"><span>7</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-8-{{$index}}" name="answers_{{$index}}" 
                                        value="8">
                                        <label for="rating-8-{{$index}}"><span>8</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-9-{{$index}}" name="answers_{{$index}}" 
                                        value="9">
                                        <label for="rating-9-{{$index}}"><span>9</span></label>
                                    </div>
                                    <div>
                                        <input type="radio" id="rating-10-{{$index}}" name="answers_{{$index}}" 
                                        value="10">
                                        <label for="rating-10-{{$index}}"><span>10</span></label>
                                    </div>
                                </div>
                                @if ($errors->has("answers_1") || $errors->has("answers_2") || $errors->has("answers_3") ||
                                    $errors->has("answers_4") || $errors->has("answers_5") || $errors->has("answers_6") ||
                                    $errors->has("answers_7"))
                                    <span class="text-danger">
                                    <strong>All the question should be answered!!</strong>
                                    </span>
                                @endif
                            </div>
                            <span hidden>{{$index++}}</span>
                        @endforeach
                        <hr>
                        <div class="form-group mt-3">
                            <button type="submit" class="btn btn-lg btn-danger form-control float-right">Submit</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
