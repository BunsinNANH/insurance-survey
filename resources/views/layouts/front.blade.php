<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset("/css/styles.css") }}">
    <link rel="stylesheet" href="{{ asset("/fonts/css/font.css") }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Post Digital Insurance Survey Facilities</title>
</head>
<body>
    <div class="container-fluid top-banner align-items-center d-flex justify-content-center sticky-top">
        <h1 class="text-center text-light">
            Post Digital Insurance Survey Facilities
        </h1>
    </div>
    @yield('frontpage')
</body>
@stack('scripts')
</html>