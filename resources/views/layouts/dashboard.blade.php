<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Dashboard - Admin</title>
        <link href="{{ asset("css/dashboard.css") }}" rel="stylesheet">
        <link href="{{ asset('/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="sb-nav-fixed">
        <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
            <!-- Navbar Brand-->
            <a class="navbar-brand ps-3" href="{{ url("dashboard") }}">
                <h2>Insurance Survey</h2>
            </a>
            <!-- Navbar-->
            <div class="collapse navbar-collapse justify-content-end" id="navbarCollapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link">
                            <p class="text-light">
                                <b>{{ Auth::user()->name }}</b>
                            </p>
                        </a>
                    </li>
                  <li class="nav-item">
                    <a class="nav-link" href="{{ route("logout") }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Sign Out</a>
                  </li>
                </ul>
            </div>
        </nav>
        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                    <div class="sb-sidenav-menu">
                        <div class="nav">
                            <a class="nav-link {{ Request::is("dashboard") ? "active" : null }}" href="{{ url("dashboard") }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                                Dashboard
                            </a>
                            <a class="nav-link {{ Request::is("company") ? "active" : null }}" href="{{ url("company") }}">
                                <div class="sb-nav-link-icon">
                                    <i class="fa fa-university" aria-hidden="true"></i>
                                </div>
                                Insurance Companies
                            </a>
                            <a class="nav-link {{ Request::is("question") ? "active": null }}" href="{{ url("question") }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-question-circle"></i></div>
                                Questions
                            </a>
                            <a class="nav-link {{ Request::is("respondents") ? "active" : null }}" href="{{ url("respondents") }}">
                                <div class="sb-nav-link-icon"><i class="fas fa-users"></i></div>
                                Respondent
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
            <div id="layoutSidenav_content">
                <main>
                    <div class="container-fluid">
                    	@yield('content')
                    </div>
                </main>
            </div>
        <form id="logout-form" action="{{ route("logout") }}" method="POST" style="display: none;">
            @csrf
        </form>
    </body>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
          $(".alert").fadeTo(2000, 500).slideUp(500, function() {
              $(".alert").slideUp(500);
          });
          $('.table').DataTable();
        });
    </script>
    @stack('scripts')
</html>
