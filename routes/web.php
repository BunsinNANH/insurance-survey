<?php
use App\Models\Question;
use App\Models\InsuranceCompany;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\DeleteCompanyController;
use App\Http\Controllers\DeleteQuestionController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\RespondentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $index = 1;
    $companies = InsuranceCompany::orderBy("company_name",'asc')->get();
    $questions = Question::orderBy("question",'asc')->where("active",1)->get();
    return view('home',compact('companies','questions','index'));
});
Route::get('survey-success',function(){
    return view('success');
});
Route::get("admin",function(){
    return redirect("dashboard");
});
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('insurance-survey', SurveyController::class);
Route::resource('company', CompanyController::class);
Route::resource('question', QuestionController::class);
Route::resource('dashboard', DashboardController::class);
Route::resource('deleteCompany', DeleteCompanyController::class);
Route::resource('deleteQuestion', DeleteQuestionController::class);
Route::resource('respondents', RespondentController::class);