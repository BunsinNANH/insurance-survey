<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_company', function (Blueprint $table) {
            $table->id();
            $table->string("company_name");
            $table->string("company_logo")->default("company.png");
            $table->string("company_short_name")->nullable();
            $table->timestamps();
        });
        $companies =[
            "AIA (Cambodia) Life Insurance Plc",
            "Prudential (Cambodia) Life Assurance PLC",
            "Manulife Cambodia",
            "Infinity Insurance",
            "Cambodian National Insurance Company",
            "SOVANNAPHUM Life Assurance PLC.",
            "Asian Insurance (Cambodia) Plc",
        ];
        for ($i=0; $i < count($companies) ; $i++) { 
            DB::table('insurance_company')->insert([
                'id' => $i+1,
                'company_name' => $companies[$i],
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_company');
    }
}
