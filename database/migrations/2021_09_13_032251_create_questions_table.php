<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string("question");
            $table->timestamps();
        });
        $questions =[
            "Is a company I can trust",
            "Has a good reputation",
            "Has a long history of serving customer well",
            "Offers financial products that sult my life",
            "Has friendly representatives who are knowledgeable and professional",
            "A company which friends and family talk positively about",
            "Is a company that seems to care about khmer people",
        ];
        for ($i=0; $i < count($questions) ; $i++) { 
            DB::table('questions')->insert([
                'id' => $i+1,
                'question' => $questions[$i],
                'created_at' => Carbon\Carbon::now(),
                'updated_at' => Carbon\Carbon::now(),
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
